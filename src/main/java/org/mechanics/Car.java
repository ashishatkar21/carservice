package org.mechanics;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Car {

    @Id
    private int cid;
    private String cmodel;
    private String ctype;
    private String cregno;
    private int cfuellevel;
    @ManyToOne
    private Customer customer;

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getCmodel(String i10_grand) {
        return cmodel;
    }

    public void setCmodel(String cmodel) {
        this.cmodel = cmodel;
    }

    public String getCtype(String hatchback) {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }

    public String getCregno() {
        return cregno;
    }

    public void setCregno(String cregno) {
        this.cregno = cregno;
    }

    public int getCfuellevel() {
        return cfuellevel;
    }

    public void setCfuellevel(int cfuellevel) {
        this.cfuellevel = cfuellevel;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Car{" +
                "cid=" + cid +
                ", cmodel='" + cmodel + '\'' +
                ", ctype='" + ctype + '\'' +
                ", cregno='" + cregno + '\'' +
                ", cfuellevel=" + cfuellevel +
                '}';
    }
}
