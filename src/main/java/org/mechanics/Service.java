package org.mechanics;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Service {


    public void carService(int car_id){
        System.out.println("Car service done");


        Configuration con = new Configuration().configure().addAnnotatedClass(Customer.class).addAnnotatedClass(Car.class);

        SessionFactory sf = con.buildSessionFactory();
        Session session = sf.openSession();

        Transaction tx = session.beginTransaction();
        Car cc = session.load(Car.class,car_id);
        cc.setCfuellevel(100);
        session.update(cc);

        tx.commit();

        System.out.println("Fuel Level Updated");

    }

}
