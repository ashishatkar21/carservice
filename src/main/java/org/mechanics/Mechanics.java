package org.mechanics;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.BootstrapServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.util.Collection;
import java.util.Scanner;


public class Mechanics {

    public static void main(String[] args){

        int customer_id;
        System.out.println("******WELCOME******");
        System.out.println("Enter Customer ID :");

        Scanner sc= new Scanner(System.in);
        customer_id= sc.nextInt();

        Configuration con = new Configuration().configure().addAnnotatedClass(Customer.class).addAnnotatedClass(Car.class);

        SessionFactory sf = con.buildSessionFactory();
        Session session = sf.openSession();

        Transaction tx = session.beginTransaction();

        Customer c = session.get(Customer.class,customer_id);
        System.out.println("Customer Details :");
        System.out.println(c);
        Collection<Car> car = c.getCar();
        for(Car cl : car)
        {
            System.out.println(cl);
        }


        tx.commit();

        System.out.println("Enter car ID which needs to be serviced :");
        int car_id = sc.nextInt();

        Service scar = new Service();
        scar.carService(car_id);


    }

}
